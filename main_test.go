package main

import (
	"reflect"
	"testing"
)

func Test_sortStringArrayByACount(t *testing.T) {
	type args struct {
		stringArray []string
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		// TODO: Add test cases.
		{"testWithDifferentAcount", args{[]string{"aaaasd", "a", "aab", "aaabcd", "ef", "cssssssd", "fdz", "kf", "zc", "lklklklklklklklkl", "l"}}, []string{"aaaasd", "aaabcd", "aab", "a", "lklklklklklklklkl", "cssssssd", "fdz", "zc", "kf", "ef", "l"}},
		{"testWithSameAcount", args{[]string{"aaaasd", "a", "aab", "aaabcd", "aaabcdy", "ef", "cssssssd", "fdz", "kf", "zc", "lklklklklklklklkl", "l"}}, []string{"aaaasd", "aaabcdy", "aaabcd", "aab", "a", "lklklklklklklklkl", "cssssssd", "fdz", "kf", "zc", "ef", "l"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := sortStringArrayByACount(tt.args.stringArray); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("sortStringArrayByACount() = %v, want %v", got, tt.want)
			}
		})
	}
}
