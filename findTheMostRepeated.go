package main

func findTheMostRepeated(stringArray []string) string { //assuming there will be no values having the same amount of occurrences.
	freqMap := map[string]int{} //map to keep the count of values
	maxCount := 0
	var mostRepeatedValue string

	for _, val := range stringArray {
		freqMap[val]++               // increase value by one when you see it
		if freqMap[val] > maxCount { // max count should be updated when a repeated value larger than maxCount variable
			maxCount = freqMap[val]
			mostRepeatedValue = val
		}
	}

	return mostRepeatedValue
}
