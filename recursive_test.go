package main

import "testing"

func Test_recursive(t *testing.T) {
	type args struct {
		n int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		// TODO: Add test cases.
		{"test1", args{9}, 0},
		{"test1", args{25}, 0},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := recursive(tt.args.n); got != tt.want {
				t.Errorf("recursive() = %v, want %v", got, tt.want)
			}
		})
	}
}
