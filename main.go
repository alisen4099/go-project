package main

import (
	"sort"
	"strings"
)

func sortStringArrayByACount(stringArray []string) []string {
	aCountPlusLenghtOfStrings := make(map[string]int)

	for i := 0; i < len(stringArray); i++ { // add "a" count to len, so it will be bigger when "a" count will be the same but the length is longer
		if strings.Contains(stringArray[i], "a") {
			aCountPlusLenghtOfStrings[stringArray[i]] = len(stringArray[i]) + strings.Count(stringArray[i], "a")
		} else { // set a count to minus value, so it will be easier to sort
			aCountPlusLenghtOfStrings[stringArray[i]] = -1000 + len(stringArray[i])
		}
	}

	keys := make([]string, 0, len(aCountPlusLenghtOfStrings))
	for k := range aCountPlusLenghtOfStrings {
		keys = append(keys, k)
	}
	// sort map by value
	sort.SliceStable(keys, func(i, j int) bool {
		return aCountPlusLenghtOfStrings[keys[i]] > aCountPlusLenghtOfStrings[keys[j]]
	})

	sortedArray := make([]string, len(stringArray))
	index := 0
	//create an array from final sorted map
	for _, k := range keys {
		sortedArray[index] = k
		index++
	}
	return sortedArray

}
func main() {
	//strs := []string{"aaaasd", "a", "aab", "aaabcd", "aaabcdy", "ef", "cssssssd", "fdz", "kf", "zc", "lklklklklklklklkl", "l"}
	//sortStringArrayByACount(strs)

}
