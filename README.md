# go-project
# How to run tests
Run the following command in go-project
`go test main_test.go main.go`

Run the command below to run the tests in **recursive.go** file
`go test recursive_test.go recursive.go main.go`

Run the command below to run the tests in **findTheMostRepeated.go** file
`go test findTheMostRepeated_test.go findTheMostRepeated.go main.go `

## Project Structure
Project structure kept simple since this is a small project.


## Notes
For the tests under findTheMostRepeated_test.go file, these tests are failing due to the unconsistency about the returning list when there is no "a" in the string 
and the lengths are the same. Conditions that the question asking me to do, is done perfectly.
