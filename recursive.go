package main

import (
	"fmt"
	"math"
)

// example output 2 4 9 when function takes 9
// assuming starting from 9 and subtraction the ceiled division by two to continue
func recursive(n int) int {
	if n < 2 { //termination condition
		return 0
	}
	recursive(n - int(math.Ceil((float64(n) / float64(2)))))
	fmt.Println(n)
	return 0
}
