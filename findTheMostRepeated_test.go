package main

import "testing"

func Test_findTheMostRepeated(t *testing.T) {
	type args struct {
		stringArray []string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
		{"test", args{[]string{"apple", "pie", "apple", "red", "red", "red"}}, "red"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := findTheMostRepeated(tt.args.stringArray); got != tt.want {
				t.Errorf("findTheMostRepeated() = %v, want %v", got, tt.want)
			}
		})
	}
}
